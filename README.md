# Container registry

Goals:
 1. Built docker images for CI jobs for hiera, puppet, ansible-infra. Images are generated weekly.
 1. Publish upstream container images downstream without [upstream container registry traffic limits](https://www.docker.com/increase-rate-limits). See below how to trigger a release.

## How to release particular upstream image downstream

Tag this repository according one of schemes:
 * release downstream specific tag only `<upstream-app-name>/<upstream-app-container-tag/version>`, see examples:
   * to release `prom/node-exporter:v1.1.2` to `registry.gitlab.ics.muni.cz:443/cloud/container-registry/node-exporter:v1.1.2` use tag [`node-exporter/v1.1.2`](https://gitlab.ics.muni.cz/cloud/container-registry/-/tags/node-exporter%2Fv1.1.2)
   * to release `gcr.io/cadvisor/cadvisor:v0.39.0` to `registry.gitlab.ics.muni.cz:443/cloud/container-registry/cadvisor:v0.39.0` use tag [`cadvisor/v0.39.0`](https://gitlab.ics.muni.cz/cloud/container-registry/-/tags/cadvisor%2Fv0.39.0)
   * to release `cytopia/ansible-lint:5-0.6` to `registry.gitlab.ics.muni.cz:443/cloud/container-registry/ansible-lint:5-0.6` use tag [`ansible-lint/5-0.6`](https://gitlab.ics.muni.cz/cloud/container-registry/-/tags/ansible-lint%2F5-0.6)
 * release downstream specific tag and additional downstream tags `<upstream-app-name>/<upstream-app-container-tag/version>/<downstream-app-container-tag>[/<downstream-app-container-tag>]` see examples:
   * `docker/19.03.15/19/latest`

