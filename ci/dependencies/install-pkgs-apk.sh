#!/bin/sh
# 
# install-pkgs-apk.sh <pkg-requirement-file> ... [pkg-repository-url] ...  [package-name] ...
# 
# installs linux packages from various sources
# * <pkg-requirement-file> i.e. packages listed in text file
# * [pkg-repository-url]   i.e. enable foreign package repository
# * [package-name]         i.e. explicitly listed packages
#
# Note: Only Alpine-like Linux distros with apk are supported at the moment

set -eo pipefail

APT='apk'

function install_pkgs_from_requirement_file() {
    local file="$1"
    ${APT} add $(grep -v '^#' "${file}")
}

function install_pkg_repository() {
    local repo_url="$1"
    echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> "/etc/apk/repositories"
    yum-config-manager --add-repo "${repo_url}"
}

function install_pkgs() {
    ${APT} add "$@"
}

apk update

for i_arg in "$@"; do
  if [ -r "${i_arg}" ]; then
      install_pkgs_from_requirement_file "${i_arg}"
  elif [[ "${i_arg}" =~ https?://.+ ]]; then
      install_pkg_repository "${i_arg}"
  else
      install_pkgs "${i_arg}"
  fi
done
