#!/bin/bash

# Parse arguments and use them in a for loop
# For loop does pull, tag, push on docker images into container registry
# Usage: ./build_pull_tag_push.sh --images "${IMAGES}" --container-image-registry-namespace ${CONTAINER_IMAGE_REGISTRY_NAMESPACE}

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    --images)
      IMAGES="$2"
      shift # past argument
      shift # past value
      ;;
    --container-image-registry-namespace)
      CIRN="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

echo "IMAGES  = ${IMAGES}"
echo "CONTAINER-IMAGE-REGISTRY-NAMESPACE     = ${CIRN}"

for i_image_tag in ${IMAGES}; do
  echo "Releasing image ${i_image_tag} as ${CIRN}/${i_image_tag}"
  export DOCKER_CONTENT_TRUST=1
  if ! docker pull "${i_image_tag}"; then
    export DOCKER_CONTENT_TRUST=0
    docker pull "${i_image_tag}"
  fi
  docker tag "${i_image_tag}" "${CIRN}/${i_image_tag}"
  export DOCKER_CONTENT_TRUST=0
  docker push "${CIRN}/${i_image_tag}"
done
